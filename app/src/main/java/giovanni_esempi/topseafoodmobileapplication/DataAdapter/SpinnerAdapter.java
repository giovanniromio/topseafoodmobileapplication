package giovanni_esempi.topseafoodmobileapplication.DataAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import giovanni_esempi.topseafoodmobileapplication.Models.Confezione;
import giovanni_esempi.topseafoodmobileapplication.R;

/**
 * Created by Giovanni on 08/02/2016.
 */
public class SpinnerAdapter extends ArrayAdapter<Confezione> {

    private List<Confezione> confezioni;

    public SpinnerAdapter(Context context, List<Confezione> objects) {
        super(context, 0, objects);
        confezioni = objects;

    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        return getCustomView(pos, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_layout, parent, false);
            holder = new ViewHolder();
            holder.descrizione = (TextView) convertView.findViewById(R.id.textView2);
            holder.descrizione.setText("ID: " + confezioni.get(position).getId() + "Pezzi per confezione: " + confezioni.get(position).getQuantity());
        }

        return convertView;

    }

    private class ViewHolder {
        protected TextView descrizione;
    }


}
