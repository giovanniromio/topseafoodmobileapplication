package giovanni_esempi.topseafoodmobileapplication.DataAdapter;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import giovanni_esempi.topseafoodmobileapplication.Models.Confezione;
import giovanni_esempi.topseafoodmobileapplication.Models.Product;
import giovanni_esempi.topseafoodmobileapplication.R;

/**
 * Created by Giovanni on 27/01/2016.
 */
public class ProductAdapter extends ArrayAdapter<Product> {


    public ProductAdapter(Context context, List<Product> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_product, parent, false);

            holder = new ViewHolder();

            holder.description = (TextView) convertView.findViewById(R.id.product_description);

            holder.price = (TextView) convertView.findViewById(R.id.product_price);

            holder.image = (ImageView) convertView.findViewById(R.id.product_image);

            holder.addToShoppingCart = (ImageButton) convertView.findViewById(R.id.product_addtoshoppingcart);

            holder.listConfezione = (Spinner) convertView.findViewById(R.id.spinner);


            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Product product = getItem(position);

        holder.image.setImageBitmap(product.getImmagine());
        holder.description.setText(product.getDescription());

        holder.price.setText("Prezzo/kg:" + product.getPrice() + "€");
        holder.image.setImageBitmap(product.getImmagine());

        final SpinnerAdapter spinnerAdapter;

        if (product.getAllConfezioni().size() != 0) {

            spinnerAdapter = new SpinnerAdapter(convertView.getContext(), product.getAllConfezioni());

            holder.listConfezione.setAdapter(spinnerAdapter);

        } else {
            spinnerAdapter = new SpinnerAdapter(convertView.getContext(), null);

        }

        holder.addToShoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Add selected Confezione in carrello
                DataManager.getInstanceOf().addConfezioneInCarrello((Confezione) holder.listConfezione.getSelectedItem());
                //Remove selected Confezione from adapter
                product.getAllConfezioni().remove((Confezione) holder.listConfezione.getSelectedItem());
                //Notify the adapter
                spinnerAdapter.notifyDataSetChanged();
                //Fast notify to the ListView Item -> spinner
                holder.listConfezione.setAdapter(spinnerAdapter);

            }
        });

        return convertView;
    }

    private class ViewHolder {
        //Fileds to Display can be extended
        protected TextView description;
        protected TextView price;
        protected ImageView image;
        protected Spinner listConfezione;
        protected ImageButton addToShoppingCart;

    }
}