package giovanni_esempi.topseafoodmobileapplication.DataAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import giovanni_esempi.topseafoodmobileapplication.Models.Confezione;
import giovanni_esempi.topseafoodmobileapplication.R;

/**
 * Created by Giovanni on 08/02/2016.
 */
public class ConfectionAdapter extends ArrayAdapter<Confezione> {

    public ConfectionAdapter(Context context, List<Confezione> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_shopping_cart, parent, false);
            holder = new ViewHolder();

            holder.productID = (TextView) convertView.findViewById(R.id.textViewProductID);
            holder.confectionID = (TextView) convertView.findViewById(R.id.textViewConfezioneID);
            holder.dataScadenza = (TextView) convertView.findViewById(R.id.textViewDataScandeza);
            holder.price = (TextView) convertView.findViewById(R.id.textViewCosto);
            holder.quantity = (TextView) convertView.findViewById(R.id.textViewQuantita);
            holder.subTotal = (TextView) convertView.findViewById(R.id.textViewSubtotale);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        Confezione confezione = getItem(position);

        holder.productID.setText("Prodotto: " + confezione.getProductName());
        holder.confectionID.setText("Confezione: " + confezione.getId());
        holder.dataScadenza.setText("Data di scadenza: " + confezione.getData());
        holder.price.setText("Costo: " + confezione.getPrice() + " €");
        holder.quantity.setText("Quantità: " + "" + confezione.getQuantity());
        holder.subTotal.setText("Subtotale: " + (confezione.getQuantity() * confezione.getPrice()) + " €");

        return convertView;

    }

    private class ViewHolder {

        protected TextView productID;

        protected TextView confectionID;

        protected TextView dataScadenza;

        protected TextView price;

        protected TextView quantity;

        protected TextView subTotal;

    }
}
