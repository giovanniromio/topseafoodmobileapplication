package giovanni_esempi.topseafoodmobileapplication.DataAdapter;

import java.util.ArrayList;
import java.util.List;

import giovanni_esempi.topseafoodmobileapplication.Models.ClienteFornitore;
import giovanni_esempi.topseafoodmobileapplication.Models.Confezione;

/**
 * Created by Giovanni on 23/10/2015.
 */
public class DataManager {

    private static List<Confezione> confezioneInCarrello;
    private static DataManager myDataManager;
    private ClienteFornitore cliente;

    public static DataManager getInstanceOf() {
        if (myDataManager == null) {
            myDataManager = new DataManager();
            confezioneInCarrello = new ArrayList<>();
        }
        return myDataManager;
    }

    public void addCliente(ClienteFornitore clienteFornitore) {
        cliente = clienteFornitore;
    }

    public ClienteFornitore getCliente() {
        return cliente;
    }

    public void addConfezioneInCarrello(Confezione cf) {
        confezioneInCarrello.add(cf);
    }

    public List<Confezione> getConfezioniInCarrello() {
        return confezioneInCarrello;
    }

}
