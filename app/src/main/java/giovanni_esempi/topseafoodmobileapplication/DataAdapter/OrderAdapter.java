package giovanni_esempi.topseafoodmobileapplication.DataAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import giovanni_esempi.topseafoodmobileapplication.Models.Order;
import giovanni_esempi.topseafoodmobileapplication.R;

/**
 * Created by Giovanni on 22/02/2016.
 */
public class OrderAdapter extends ArrayAdapter<Order> {

    public OrderAdapter(Context context, List<Order> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_orders, parent, false);
            holder = new ViewHolder();

            holder.idOrder = (TextView) convertView.findViewById(R.id.txtOrder);

            holder.date = (TextView) convertView.findViewById(R.id.txtDate);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Order order = getItem(position);

        holder.idOrder.setText("IdOrdine: " + order.getIdOrdine());
        holder.date.setText("Data ordine: " + order.getDataOrdine().toString());

        return convertView;
    }

    private class ViewHolder {
        //Fileds to Display can be extended
        protected TextView idOrder;
        protected TextView date;
    }


}

