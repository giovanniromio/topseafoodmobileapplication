package giovanni_esempi.topseafoodmobileapplication.DataAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import giovanni_esempi.topseafoodmobileapplication.Models.Category;
import giovanni_esempi.topseafoodmobileapplication.R;

/**
 * Created by Giovanni on 30/11/2015.
 */
public class CategoryAdapter extends ArrayAdapter<Category> {

    public CategoryAdapter(Context context, List<Category> categories) {
        super(context, 0, categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.grid_item_category, parent, false);
            holder = new ViewHolder();
            holder.description = (TextView) convertView.findViewById(R.id.textViewNomeCategoria);
            holder.immagine = (ImageView) convertView.findViewById(R.id.imageViewCategoria);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Category category = getItem(position);

        holder.immagine.setImageBitmap(category.getImmagine());
        holder.description.setText("" + category.getDescription());

        return convertView;
    }

    private class ViewHolder {
        //Fileds to Display can be extended
        TextView description;
        ImageView immagine;
    }


}
