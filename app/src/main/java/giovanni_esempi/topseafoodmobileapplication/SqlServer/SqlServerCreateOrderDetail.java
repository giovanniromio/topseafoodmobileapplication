package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Giovanni on 22/02/2016.
 */
public class SqlServerCreateOrderDetail extends SqlServer {

    @Override
    protected Void doInBackground(String... params) {
        try {
            Statement stmt = SqlServerSingleton.getInstanceOf().getCurrentConnection().createStatement();
            stmt.executeQuery(params[0]);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (controller != null) {
            controller.notifyController(true);
        }
    }
}
