package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.Models.Category;

/**
 * Created by Giovanni on 05/02/2016.
 */
public class SqlServerCategorie extends SqlServer {

    private final int ID = 1;
    private final int DESCRIZIONE = 2;
    private final int ABBREVIAZIONE = 3;
    private final int IMMAGINE = 4;

    private ArrayList<Category> categories;

    @Override
    protected Void doInBackground(String... params) {
        try {
            Statement stmt = SqlServerSingleton.getInstanceOf().getCurrentConnection().createStatement();
            //Execute the query
            ResultSet resultSet = stmt.executeQuery(params[0]);

            categories = new ArrayList<>();

            while (resultSet.next()) {

                Category currentCategory = new Category();
                //Imposta L'ID
                currentCategory.setId(Integer.parseInt(resultSet.getString(ID)));
                //Imposta la descrizione della categoria
                currentCategory.setDescription(resultSet.getString(DESCRIZIONE));
                //Imposta l'abbreviazione della categoria
                currentCategory.setAbbreviazione(resultSet.getString(ABBREVIAZIONE));
                //Scarica l'immagine
                int blobLength = (int) resultSet.getBlob(IMMAGINE).length();
                byte[] blobAsBytes = resultSet.getBlob(IMMAGINE).getBytes(1, blobLength);
                //Estrae l'immagine da un array
                currentCategory.setImmagine(decodeSampledBitmapFromResource(blobAsBytes, 0, blobLength, 100, 100));
                //TODO ADD PLACE HOLDER TO CATEGORIES WITH NO IMAGE
                categories.add(currentCategory);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    protected void onPostExecute(Void result) {
        if (controller != null) {
            controller.notifyController(categories);
        }
    }


}
