package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import giovanni_esempi.topseafoodmobileapplication.DataAdapter.DataManager;
import giovanni_esempi.topseafoodmobileapplication.Models.ClienteFornitore;

/**
 * Created by Giovanni on 05/02/2016.
 */
public class SqlServerLogin extends SqlServer {

    private final int ID = 1;
    private final int NOME_REFERENTE = 4;
    private final int COGNOME_REFERENTE = 5;
    private final int TELEFONO = 6;
    private final int VIA = 7;
    private final int CAP = 9;
    private final int CODICEFISCALE = 10;
    private final int USERNAME = 15;
    private final int PASSWORD = 16;

    @Override
    protected Void doInBackground(String... params) {

        try {

            Statement stmt = SqlServerSingleton.getInstanceOf().getCurrentConnection().createStatement();
            ResultSet resultSet = stmt.executeQuery(params[0]);

            while (resultSet.next()) {

                ClienteFornitore cf = new ClienteFornitore();
                cf.setid(Integer.parseInt(resultSet.getString(ID)));
                cf.setNome_referente(resultSet.getString(NOME_REFERENTE));
                cf.setCognome_referente(resultSet.getString(COGNOME_REFERENTE));
                cf.setTelefono(resultSet.getString(TELEFONO));
                cf.setVia(resultSet.getString(VIA));
                cf.setCAP(resultSet.getString(CAP));
                cf.setCodice_fiscale(resultSet.getString(CODICEFISCALE));
                cf.setUsername(resultSet.getString(USERNAME));
                cf.setPassword(resultSet.getString(PASSWORD));

                //Aggiunge il nuovo ClienteFornitore al DataManager
                DataManager.getInstanceOf().addCliente(cf);

                break;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    protected void onPostExecute(Void result) {

        if (controller != null) {
            controller.notifyController(true);
        }
    }

}


