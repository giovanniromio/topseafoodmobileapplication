package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.Models.Order;

/**
 * Created by Giovanni on 22/02/2016.
 */
public class SqlServerGetAllOrders extends SqlServer {

    private ArrayList<Order> orderList;

    @Override
    protected Void doInBackground(String... params) {
        try {
            Statement stmt = SqlServerSingleton.getInstanceOf().getCurrentConnection().createStatement();
            ResultSet resultSet = stmt.executeQuery(params[0]);

            orderList = new ArrayList<>();

            while (resultSet.next()) {
                Order order = new Order();
                order.setDataOrdine(resultSet.getDate(2));
                order.setIdOrdine(Integer.parseInt(resultSet.getString(1)));
                orderList.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        controller.notifyController(orderList);
    }

}
