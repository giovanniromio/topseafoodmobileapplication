package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import java.sql.Statement;

/**
 * Created by Giovanni on 05/02/2016.
 */
public class SqlServerSignup extends SqlServer {

    @Override
    protected Void doInBackground(String... params) {
        try {
            Statement stmt = SqlServerSingleton.getInstanceOf().getCurrentConnection().createStatement();
            //Execute the query
            stmt.executeQuery(params[0]);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        if (controller != null) {
            controller.notifyController(true);
        }
    }

}
