package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.Controller.Controller;

/**
 * Created by Giovanni on 04/02/2016.
 */
public abstract class SqlServer extends AsyncTask<String, Void, Void> {
    protected Controller controller;

    @Override
    protected Void doInBackground(String... params) {
        return null;
    }

    public void attachObserver(Controller controller) {
        this.controller = controller;
    }

    @Override
    protected void onPostExecute(Void result) {
    }

    public Bitmap decodeSampledBitmapFromResource(byte[] blobAsBytes, int i, int blobLength, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(blobAsBytes, i, blobLength, options);
    }

    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public interface ControllerObserver<X> {

        public void notifyController(Boolean result);

        public void notifyController(ArrayList<X> values);

        public void connectionFailed();

    }


}
