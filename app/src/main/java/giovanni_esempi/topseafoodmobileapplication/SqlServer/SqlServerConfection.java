package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import giovanni_esempi.topseafoodmobileapplication.Models.Confezione;
import giovanni_esempi.topseafoodmobileapplication.Models.Product;

/**
 * Created by Giovanni on 04/02/2016.
 */
public class SqlServerConfection extends SqlServer {

    private final int CONFEZIONEID = 1;
    private final int DATASCADENZA = 3;
    private final int QUANTITA = 4;

    private Product product;
    private int cont;

    public SqlServerConfection(Product product) {
        this.product = product;
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            Statement stmt = SqlServerSingleton.getInstanceOf().getCurrentConnection().createStatement();
            ResultSet resultSet = stmt.executeQuery(params[0]);

            while (resultSet.next()) {

                Confezione cf = new Confezione();
                cf.setId(resultSet.getInt(CONFEZIONEID));
                cf.setQuantity(resultSet.getInt(QUANTITA));
                cf.setData(resultSet.getDate(DATASCADENZA).toString());
                cf.setPrice(product.getPrice());
                cf.setProductName(product.getDescription());
                product.addConfezione(cf);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPostExecute(Void result) {
        if (controller != null) {
            controller.notifyController(true);
        }
    }


}
