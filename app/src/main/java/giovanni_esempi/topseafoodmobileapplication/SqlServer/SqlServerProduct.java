package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.Models.Product;

/**
 * Created by Giovanni on 04/02/2016.
 */
public class SqlServerProduct extends SqlServer {

    private static int PRODUCTID = 1;
    private static int PRODUCTDESCRIPTION = 2;
    private static int PRODUCTPRICE = 4;
    private static int PRODUCTIMAGE = 6;

    private ArrayList<Product> products;

    @Override
    protected Void doInBackground(String... params) {
        try {
            Statement stmt = SqlServerSingleton.getInstanceOf().getCurrentConnection().createStatement();
            //Execute the query
            ResultSet resultSet = stmt.executeQuery(params[0]);

            products = new ArrayList<>();

            while (resultSet.next()) {

                //FETCH PRODOTTI
                Product currentProduct = new Product();
                currentProduct.setID(resultSet.getString(PRODUCTID));
                currentProduct.setDescription(resultSet.getString(PRODUCTDESCRIPTION));
                currentProduct.setPrice(Double.parseDouble(resultSet.getString(PRODUCTPRICE)));

                //SET IMMAGINE
                try {
                    int blobLength = (int) resultSet.getBlob(PRODUCTIMAGE).length();
                    byte[] blobAsBytes = resultSet.getBlob(PRODUCTIMAGE).getBytes(1, blobLength);
                    //Estrae l'immagine da un array
                    //  BitmapFactory.Options bitmapOption = new BitmapFactory.Options();
                    // bitmapOption.inBitmap = BitmapFactory.decodeByteArray(blobAsBytes, 0, blobLength);
                    currentProduct.setImmagine(decodeSampledBitmapFromResource(blobAsBytes, 0, blobLength, 100, 100));

                    //BitmapFactory.decodeByteArray(blobAsBytes, 0, blobLength);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                    //TODO se non c'è l'immagine nel server usare un placeholder!!!!
                }
                products.add(currentProduct);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {

        if (controller != null) {
            controller.notifyController(products);
        }
    }


}
