package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Giovanni on 04/02/2016.
 */
public class SqlServerSingleton {

    private static final String IP = "10.0.2.2:1433/";
    //private static final String IP = "127.0.0.1:1433/";

    //private static final String IP = "192.168.1.150:1433/";
    private static SqlServerSingleton sqlServer;
    private final String DBNAME = "mycompany";
    private final String USERNAME = "653563";

    //    private final String IP = "87.11.3.51:1433/";
    private final String PASSWORD = "topseafood";
    protected Connection currentConnection;
    protected String driver = "net.sourceforge.jtds.jdbc.Driver";
    private SqlServer sqlServerController;


    private SqlServerSingleton() {
        connect();
    }

    public static SqlServerSingleton getInstanceOf() {
        if (sqlServer == null) {
            return sqlServer = new SqlServerSingleton();
        } else {
            return sqlServer;
        }

    }

    protected void connect() {
        Log.i("Android", " MySQL Connect Example.");
        if (currentConnection == null) {
            try {
                Class.forName(driver).newInstance();
                //OLD String connString = "jdbc:jtds:sqlserver://server_ip_address :1433/mycompany;encrypt=fasle;user=653563;password=topseafood;instance=SQLEXPRESS;
                String connString = "jdbc:jtds:sqlserver://" + IP + "/" + DBNAME + ";encrypt=true" + ";user=" + USERNAME + ";password="
                        + PASSWORD + ";";
                currentConnection = DriverManager.getConnection(connString);
                Log.w("Connection", "open");
            } catch (Exception e) {
                // Toast.makeText(this.context, "Failed to connect to server, check internet connection or Server IP!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public Connection getCurrentConnection() {
        return this.currentConnection;
    }


}
