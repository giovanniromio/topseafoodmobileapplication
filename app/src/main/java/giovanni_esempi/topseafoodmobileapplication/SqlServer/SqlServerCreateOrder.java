package giovanni_esempi.topseafoodmobileapplication.SqlServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Giovanni on 22/02/2016.
 */
public class SqlServerCreateOrder extends SqlServer {

    private final int ID = 1;
    private int orderId;

    @Override
    protected Void doInBackground(String... params) {
        try {
            Statement stmt = SqlServerSingleton.getInstanceOf().getCurrentConnection().createStatement();
            ResultSet resultSet = stmt.executeQuery(params[0]);
            while (resultSet.next()) {
                orderId = Integer.parseInt(resultSet.getString(ID));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (controller != null) {
            //send the order ID to the Controller -> CreateOrderDetails
            ArrayList orders = new ArrayList();
            orders.add(orderId);
            controller.notifyController(orders);
        }
    }


}
