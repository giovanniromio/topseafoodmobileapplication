package giovanni_esempi.topseafoodmobileapplication.Models;

import android.graphics.Bitmap;

/**
 * Created by Giovanni on 03/12/2015.
 */
public class Category {

    private int id;
    private String description;
    private String abbreviazione;
    private Bitmap immagine;

    public void setId(int id) {
        this.id = id;
    }

    public int getID() {
        return this.id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbbreviazione() {
        return this.abbreviazione;
    }

    public void setAbbreviazione(String abbreviazione) {
        this.abbreviazione = abbreviazione;
    }

    public Bitmap getImmagine() {
        return immagine;
    }

    public void setImmagine(Bitmap immagine) {
        this.immagine = immagine;
    }

    @Override
    public int hashCode() {
        return (id + description).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        Category c = (Category) obj;
        if (this.id == c.getID() && this.description.equals(c.getDescription())) {
            return true;
        } else {
            return false;
        }
    }

}
