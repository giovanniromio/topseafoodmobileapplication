package giovanni_esempi.topseafoodmobileapplication.Models;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giovanni on 19/10/2015.
 */

public class Product {

    private String id;
    private String description;
    private double price;
    private Bitmap immagine;
    private int idCategory;
    private List<Confezione> confezioni;

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Bitmap getImmagine() {
        return immagine;
    }

    public void setImmagine(Bitmap immagine) {
        this.immagine = immagine;
    }

    public int getIdCategory() {
        return this.idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public void addConfezione(Confezione f) {
        if (confezioni == null) {
            confezioni = new ArrayList<>();
            confezioni.add(f);
        } else {
            confezioni.add(f);
        }
    }

    public void removeConfezione(int index) {
        confezioni.remove(index);
    }

    public Confezione getConfezione(int ID) {
        return confezioni.get(ID);
    }

    public List<Confezione> getAllConfezioni() {
        //Ritorniamo un diverso puntatore, non quello relativo alla lista del prodotto
        return confezioni;
    }

    @Override
    public int hashCode() {
        return (id + description).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        Product p = (Product) obj;
        if (this.id.equals(p.getID()) && this.description.equals(p.getDescription())) {
            return true;
        } else {
            return false;
        }
    }
}
