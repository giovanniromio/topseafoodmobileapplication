package giovanni_esempi.topseafoodmobileapplication.Models;

/**
 * Created by Giovanni on 01/02/2016.
 */
public class Confezione {

    private String productName;

    private int id;

    private int quantity;

    private String data;

    private double price;

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Double getTotale() {
        return this.price * this.quantity;
    }


    @Override
    public int hashCode() {
        return (id) * 997;
    }

    @Override
    public boolean equals(Object obj) {
        Confezione cf = (Confezione) obj;
        if (this.id == cf.getId()) {
            return true;
        } else {
            return false;
        }
    }

}
