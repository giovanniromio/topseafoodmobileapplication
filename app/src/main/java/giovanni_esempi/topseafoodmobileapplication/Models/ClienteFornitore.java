package giovanni_esempi.topseafoodmobileapplication.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giovanni on 20/12/2015.
 */
public class ClienteFornitore {

    private int id;
    private String nome_referente;
    private String cognome_referente;
    private String ragione_sociale;
    private String codice_fiscale;
    private String telefono;
    private String CAP;
    private String via;
    private String username;
    private String password;
    private List<Order> orderList;

    public String getNome_referente() {
        return nome_referente;
    }

    public void setNome_referente(String nome_referente) {
        this.nome_referente = nome_referente;
    }

    public String getCognome_referente() {
        return this.cognome_referente;
    }

    public void setCognome_referente(String cognome_referente) {
        this.cognome_referente = cognome_referente;
    }

    public String getRagione_sociale() {
        return ragione_sociale;
    }

    public void setRagione_sociale(String ragione_sociale) {
        this.ragione_sociale = ragione_sociale;
    }

    public String getCodice_fiscale() {
        return this.codice_fiscale;
    }

    public void setCodice_fiscale(String codice_fiscale) {
        this.codice_fiscale = codice_fiscale;
    }

    public String getCAP() {
        return this.CAP;
    }

    public void setCAP(String CAP) {
        this.CAP = CAP;
    }

    public String getVia() {
        return this.via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setid(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Order> getAllOrders() {
        return this.orderList != null ? this.orderList : new ArrayList<Order>();
    }

    public void setAllOrders(List<Order> orderList) {
        this.orderList = orderList;
    }

    @Override
    public int hashCode() {
        return this.getId() * 1103;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ClienteFornitore) {
            ClienteFornitore cfToCompare = (ClienteFornitore) obj;
            return (
                    cfToCompare.getUsername().equals(this.getUsername()) &&
                            cfToCompare.getPassword().equals(this.getPassword())
            );
        }
        return false;
    }


}
