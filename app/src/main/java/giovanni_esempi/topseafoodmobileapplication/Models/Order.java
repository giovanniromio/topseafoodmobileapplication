package giovanni_esempi.topseafoodmobileapplication.Models;

import java.util.Date;

/**
 * Created by Giovanni on 22/02/2016.
 */
public class Order {

    private int idOrdine;

    private Date dataOrdine;

    private double totaleOrdine;

    public int getIdOrdine() {
        return this.idOrdine;
    }

    public void setIdOrdine(int idOrdine) {
        this.idOrdine = idOrdine;
    }

    public Date getDataOrdine() {
        return this.dataOrdine;
    }

    public void setDataOrdine(Date dataOrdine) {
        this.dataOrdine = dataOrdine;
    }

}
