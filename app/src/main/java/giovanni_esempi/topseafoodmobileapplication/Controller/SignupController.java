package giovanni_esempi.topseafoodmobileapplication.Controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServerSignup;

/**
 * Created by Giovanni on 05/02/2016.
 */
public class SignupController extends Controller<Boolean> {

    private final String QUERYRADIX = "INSERT INTO mycompany.dbo.[CLIENTE FORNITORE] () VALUES (";
    private final int QUERYCOLUMNINDEX = 10;
    private final String[] COLUMNS = {
            "Se_Cliente",
            "Se_Fornitore",
            "Nome_Referente",
            "Cognome_Referente",
            "Ragione_Sociale",
            "Recapito_Telefonico",
            "Via",
            "N_Civico",
            "Città",
            "CAP",
            "Codice_Fiscale",
            "Partita_IVA",
            "IDTipologia",
            "Username",
            "Password"
    };
    private String[] values;


    public SignupController(String[] values) {
        this.values = values;
    }

    @Override
    public void start(String query) throws SQLException {
        SqlServerSignup sqlServerSignup = new SqlServerSignup();
        sqlServerSignup.attachObserver(this);
        sqlServerSignup.execute(buildQuery());
    }

    private String buildQuery() {
        StringBuilder query = new StringBuilder(QUERYRADIX);
        String nomeColonneFormattato = (Arrays.toString(COLUMNS))
                .replace("]", "")
                .replace("[", "");
        query.insert(query.length() - QUERYCOLUMNINDEX, nomeColonneFormattato);

        for (String value : values) {
            //Inserisce gli apici all'inizo e la fine di ogni stringa
            StringBuilder tmp = new StringBuilder(value);
            if (!isInteger(value)) {
                //Se il valore è un intero non inseriamo gli apici
                tmp.insert(0, " ' ");
                tmp.append(" ' ");
            }
            //Separa tutti i valori da una virgola
            query.append(tmp + ",");
        }
        //Cancella l'ultima virgola e mette al suo posto la parentesi di chiusura
        query.replace(query.length() - 1, query.length(), ");");

        return query.toString();
    }

    public boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void notifyController(Boolean value) {
        if (value == true) {
            observer.notifyView(value);
        }
    }

    @Override
    public void notifyController(ArrayList values) {

    }

    @Override
    public void connectionFailed() {

    }
}
