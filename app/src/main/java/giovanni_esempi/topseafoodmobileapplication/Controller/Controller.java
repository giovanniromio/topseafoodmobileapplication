package giovanni_esempi.topseafoodmobileapplication.Controller;

import java.sql.SQLException;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServer;

/**
 * Created by Giovanni on 04/02/2016.
 */
public abstract class Controller<X> implements SqlServer.ControllerObserver {

    ActivityObserver observer;

    public void attachActivityObserver(ActivityObserver observer) {
        this.observer = observer;
    }

    public abstract void start(String query) throws SQLException;

    public interface ActivityObserver<Y> {

        public void notifyView(ArrayList<Y> value);

        public void notifyView(Boolean result);

        public void connectionFailed();

    }
}
