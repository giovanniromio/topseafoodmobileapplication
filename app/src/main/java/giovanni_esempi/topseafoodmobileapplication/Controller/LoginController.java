package giovanni_esempi.topseafoodmobileapplication.Controller;

import java.sql.SQLException;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServerLogin;

/**
 * Created by Giovanni on 05/02/2016.
 */
public class LoginController extends Controller {

    private String username;

    private String password;

    public LoginController(String username, String password) {

        this.username = username;
        this.password = password;
    }

    @Override
    public void start(String query) throws SQLException {
        //TODO LISTEN FOR THE SQLEXCEPTION CONNECTION FAILED
        SqlServerLogin sqlServerLogin = new SqlServerLogin();
        sqlServerLogin.attachObserver(this);
        sqlServerLogin.execute(
                "SELECT * FROM mycompany.dbo.[CLIENTE FORNITORE] WHERE Username = '" + username + "' AND Password = '" + password + "' "
        );
    }

    @Override
    public void notifyController(Boolean value) {
        observer.notifyView(value);
    }

    @Override
    public void notifyController(ArrayList values) {
    }

    @Override
    public void connectionFailed() {
    }
}
