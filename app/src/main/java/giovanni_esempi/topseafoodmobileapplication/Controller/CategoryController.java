package giovanni_esempi.topseafoodmobileapplication.Controller;

import java.sql.SQLException;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServerCategorie;

/**
 * Created by Giovanni on 05/02/2016.
 */
public class CategoryController extends Controller {
    protected SqlServerCategorie sqlServerCategorie;

    public void start(String query) throws SQLException {
        sqlServerCategorie = new SqlServerCategorie();
        sqlServerCategorie.attachObserver(this);
        sqlServerCategorie.execute(query);
    }

    @Override
    public void notifyController(ArrayList values) {
        observer.notifyView(values);
        sqlServerCategorie.cancel(true);
    }

    @Override
    public void notifyController(Boolean result) {
    }

    @Override
    public void connectionFailed() {
    }

}
