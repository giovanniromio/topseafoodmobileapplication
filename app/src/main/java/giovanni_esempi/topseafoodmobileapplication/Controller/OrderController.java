package giovanni_esempi.topseafoodmobileapplication.Controller;

import java.sql.SQLException;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.DataAdapter.DataManager;
import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServerGetAllOrders;

/**
 * Created by Giovanni on 22/02/2016.
 */
public class OrderController extends Controller {

    private String queryOrderRadix = "SELECT * FROM [dbo].[ORDINE] WHERE IDCliente_Fornitore = '";

    @Override
    public void start(String query) throws SQLException {

        int idCliente = DataManager.getInstanceOf().getCliente().getId();

        SqlServerGetAllOrders sqlServerGetAllOrders = new SqlServerGetAllOrders();
        sqlServerGetAllOrders.attachObserver(this);
        sqlServerGetAllOrders.execute(queryOrderRadix + idCliente + "'");

    }

    @Override
    public void notifyController(Boolean result) {

    }


    @Override
    public void notifyController(ArrayList values) {
        if (values.size() > 0) {
            observer.notifyView(values);
        }
    }

    @Override
    public void connectionFailed() {

    }
}
