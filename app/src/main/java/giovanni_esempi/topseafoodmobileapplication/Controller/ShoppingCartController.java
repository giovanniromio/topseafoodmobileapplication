package giovanni_esempi.topseafoodmobileapplication.Controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import giovanni_esempi.topseafoodmobileapplication.DataAdapter.DataManager;
import giovanni_esempi.topseafoodmobileapplication.Models.Confezione;
import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServerCreateOrder;
import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServerCreateOrderDetail;

/**
 * Created by Giovanni on 22/02/2016.
 */
public class ShoppingCartController extends Controller {

    @Override
    public void start(String query) throws SQLException {

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        StringBuilder stringBuilder = new StringBuilder();
        //INSERT INTO [dbo].[ORDINE] VALUES (date,0,IDCLIENTE);
        stringBuilder.append(
                "INSERT INTO [dbo].[ORDINE] ([Data_Ordine],[Se_Evaso],[IDCliente_Fornitore]) VALUES("
        );
        //date
        stringBuilder.append("'" + date + "'");
        //seEvaso
        stringBuilder.append(",'0','");
        //IDcliente
        stringBuilder.append(DataManager.getInstanceOf().getCliente().getId());
        stringBuilder.append("') SELECT SCOPE_IDENTITY()");

        SqlServerCreateOrder sqlServerCreateOrder = new SqlServerCreateOrder();
        sqlServerCreateOrder.attachObserver(this);
        sqlServerCreateOrder.execute(stringBuilder.toString());

    }

    private void iterateQueryInsert(List<Confezione> confezioneList, int orderID) {

        SqlServerCreateOrderDetail sqlServerCreateOrderDetail;
        int cont = confezioneList.size();

        for (Confezione cf : confezioneList) {
            sqlServerCreateOrderDetail = new SqlServerCreateOrderDetail();
            //Attach observer on the last call of this AsynchTask
            if (--cont == 0) {
                sqlServerCreateOrderDetail.attachObserver(this);
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("INSERT INTO [dbo].[DETTAGLI ORDINE] VALUES ('");
            stringBuilder.append(orderID);
            stringBuilder.append("'," + "'" + cf.getId() + "')");
            sqlServerCreateOrderDetail.execute(stringBuilder.toString());
        }

    }

    @Override
    public void notifyController(Boolean result) {
        //After create Order Detail
        if (observer != null) {
            observer.notifyView(true);

        }
    }

    @Override
    public void notifyController(ArrayList values) {

        iterateQueryInsert(DataManager.getInstanceOf().getConfezioniInCarrello(), (int) values.get(0));

    }

    @Override
    public void connectionFailed() {

    }
}
