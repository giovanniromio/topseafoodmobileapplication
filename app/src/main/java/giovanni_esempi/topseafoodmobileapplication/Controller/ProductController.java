package giovanni_esempi.topseafoodmobileapplication.Controller;

import java.sql.SQLException;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.Models.Product;
import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServerConfection;
import giovanni_esempi.topseafoodmobileapplication.SqlServer.SqlServerProduct;

/**
 * Created by Giovanni on 04/02/2016.
 */
public class ProductController extends Controller {

    private ArrayList<Product> products;

    private SqlServerProduct sqlServerProduct;

    private String queryStart = "SELECT * FROM mycompany.dbo.CONFEZIONE WHERE IDProdotto = '";

    private String queryEnd = "AND IDConfezione NOT IN ( SELECT IDConfezione FROM mycompany.dbo.[DETTAGLI ORDINE])";

    public void start(String query) throws SQLException {
        //TODO LISTEN FOR THE SQLEXCEPTION CONNECTION FAILED
        sqlServerProduct = new SqlServerProduct();
        sqlServerProduct.attachObserver(this);
        sqlServerProduct.execute(query);
        //sqlServerProduct.execute("SELECT * FROM mycompany.dbo.PRODOTTO");
    }

    @Override
    public void notifyController(ArrayList values) {
        if (values.size() > 0) {
            this.products = values;
            retreiveConfezioni();
        } else {
            observer.notifyView(false);
        }
    }

    private void retreiveConfezioni() {
        SqlServerConfection sqlServerConfection;
        int cont = products.size();
        for (Product p : products) {
            sqlServerConfection = new SqlServerConfection(p);
            //Attach observer on the last call of this AsynchTask
            if (--cont == 0) {
                sqlServerConfection.attachObserver(this);
            }

            /*
            SELECT * FROM mycompany.dbo.CONFEZIONE
            WHERE IDProdotto = 'GRT0000' AND
            IDConfezione NOT IN (
                    SELECT IDConfezione FROM mycompany.dbo.[DETTAGLI ORDINE])
            */

            sqlServerConfection.execute(queryStart + p.getID() + "'" + queryEnd);
        }
    }

    @Override
    public void notifyController(Boolean value) {
        observer.notifyView(products);
        sqlServerProduct.cancel(true);
    }

    @Override
    public void connectionFailed() {
    }


}
