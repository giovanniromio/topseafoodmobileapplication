package giovanni_esempi.topseafoodmobileapplication.Activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.Controller.Controller;
import giovanni_esempi.topseafoodmobileapplication.Controller.ProductController;
import giovanni_esempi.topseafoodmobileapplication.DataAdapter.ProductAdapter;
import giovanni_esempi.topseafoodmobileapplication.Models.Product;
import giovanni_esempi.topseafoodmobileapplication.R;

public class ProductActivity extends BaseActivity implements Controller.ActivityObserver<Product> {

    private final int defaultValue = -1;
    public ProductAdapter mAdapter;
    private ListView listView;
    private ProgressDialog progressDialog;
    private Controller productController;
    private int idCategory;
    private String queryAllProducts = "SELECT * FROM mycompany.dbo.PRODOTTO";

    private String querySelectProduct = "SELECT * FROM mycompany.dbo.PRODOTTO WHERE IDCategoria='";

    private String querySearchProductsRadix = "SELECT * FROM dbo.PRODOTTO WHERE Descrizione LIKE '%";

    private String querySearchProductsEnding = "%'";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        productController = new ProductController();
        productController.attachActivityObserver(this);
        //Listener for intenet on the UI
        handleIntent(getIntent());

        listView = (ListView) findViewById(R.id.listView);

        //Retreive category ID from intent

    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            String query = intent.getStringExtra(SearchManager.QUERY);
            if (query != null) {
                doMySearch(query);
                onCloseSearchWidget();
            }
        } else {
            if (intent.getIntExtra("IDCATEGORY", defaultValue) != -1) {
                try {
                    idCategory = (intent.getIntExtra("IDCATEGORY", defaultValue));
                    productController.start(querySelectProduct + idCategory + "'");
                    loadAllProducts();
                } catch (SQLException e) {
                    //TOAST MAKE TEST
                    e.printStackTrace();
                }
            } else {
                try {
                    productController.start(queryAllProducts);
                    loadAllProducts();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void doMySearch(String query) {
        try {
            productController.start(querySearchProductsRadix + query + querySearchProductsEnding);
            loadAllProducts();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadAllProducts() {
        progressDialog = new ProgressDialog(ProductActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Caricamento prodotti...");
        progressDialog.show();
    }

    private void populateListView(ArrayList<Product> products) {
        if (mAdapter == null) {
            mAdapter = new ProductAdapter(this, products);
            listView.setAdapter(mAdapter);
        } else {
            mAdapter.clear();
            mAdapter.addAll(products);
            mAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void notifyView(ArrayList<Product> values) {
        progressDialog.dismiss();
        if (values != null) {
            populateListView(values);
        }

    }

    @Override
    protected SearchView.OnCloseListener setOnCloseListener() {
        return new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                try {
                    loadAllProducts();
                    //Display all the categories
                    productController.start(querySelectProduct + idCategory + "'");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                onCloseSearchWidget();
                return true;
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void connectionFailed() {
        //TODO ADD TOAST MAKE TEST
    }

    @Override
    public void notifyView(Boolean result) {
        if (result == false) {
            progressDialog.dismiss();
            Toast.makeText(ProductActivity.this, "Nessun prodotto presente!", Toast.LENGTH_SHORT).show();
        }
    }

}
