package giovanni_esempi.topseafoodmobileapplication.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import giovanni_esempi.topseafoodmobileapplication.Controller.Controller;
import giovanni_esempi.topseafoodmobileapplication.Controller.LoginController;
import giovanni_esempi.topseafoodmobileapplication.DataAdapter.DataManager;
import giovanni_esempi.topseafoodmobileapplication.R;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements Controller.ActivityObserver {


    private static final int REQUEST_SIGNUP = 0;
    @Bind(R.id.input_email)
    EditText emailText;
    @Bind(R.id.input_password)
    EditText passwordText;
    @Bind(R.id.btn_login)
    Button loginButton;
    @Bind(R.id.link_signup)
    TextView signupLink;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (validateFields()) {
                    String username = emailText.getText().toString();
                    String password = passwordText.getText().toString();

                    LoginController loginController = new LoginController(username, password);
                    loginController.attachActivityObserver(LoginActivity.this);
                    try {
                        loginController.start("");
                        login();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

        signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
    }


    public void login() {

        loginButton.setEnabled(false);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Autenticazione in corso...");
        progressDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getBaseContext(), "Registrazione avvenuta con successo", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getBaseContext(), "Registrazione fallita", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        Toast.makeText(getBaseContext(), "Login avvenuto con successo", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);

        Intent switchActivity = new Intent(LoginActivity.this, CategoryActivity.class);
        startActivity(switchActivity);
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login fallito", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    @Override
    public void notifyView(Boolean result) {
        progressDialog.dismiss();
        if (DataManager.getInstanceOf().getCliente() != null) {
            onLoginSuccess();
        } else {
            onLoginFailed();
        }
    }

    public boolean validateFields() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("enter a valid email address");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    @Override
    public void connectionFailed() {
    }

    @Override
    public void notifyView(ArrayList values) {
    }


}
