package giovanni_esempi.topseafoodmobileapplication.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import giovanni_esempi.topseafoodmobileapplication.Controller.Controller;
import giovanni_esempi.topseafoodmobileapplication.Controller.SignupController;
import giovanni_esempi.topseafoodmobileapplication.R;

public class SignupActivity extends AppCompatActivity implements Controller.ActivityObserver<Boolean> {

    private static final String TAG = "SignupActivity";

    //Componenets Signup GUI
    @Bind(R.id.input_nome)
    EditText editText;
    @Bind(R.id.input_cognome)
    EditText cognomeText;
    @Bind(R.id.input_telephone)
    EditText telefonoText;
    @Bind(R.id.input_città)
    EditText cittaText;
    @Bind(R.id.input_cap)
    EditText capText;
    @Bind(R.id.input_ncivico)
    EditText civicoText;
    @Bind(R.id.input_email)
    EditText emailText;
    @Bind(R.id.input_password)
    EditText passwordText;
    @Bind(R.id.input_ragionesociale)
    EditText ragioneSocialeText;
    @Bind(R.id.input_via)
    EditText viaText;
    @Bind(R.id.input_cf)
    EditText codiceFiscaleText;
    @Bind(R.id.input_piva)
    EditText pivaText;
    @Bind(R.id.btn_signup)
    Button signupButton;
    @Bind(R.id.link_login)
    TextView loginLink;

    // Fields Signup GUI
    String nome;
    String cognome;
    String citta;
    String civico;
    String cap;
    String telefono;
    String email;
    String password;
    String ragioneSociale;
    String via;
    String codiceFiscale;
    String piva;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                finish();
            }
        });
    }

    public void signup() {
        Log.d(TAG, "Signup");

        nome = editText.getText().toString();
        cognome = cognomeText.getText().toString();
        citta = cittaText.getText().toString();
        civico = civicoText.getText().toString();
        cap = capText.getText().toString();
        telefono = telefonoText.getText().toString();
        email = emailText.getText().toString();
        password = passwordText.getText().toString();
        ragioneSociale = ragioneSocialeText.getText().toString();
        via = viaText.getText().toString();
        codiceFiscale = codiceFiscaleText.getText().toString();
        piva = pivaText.getText().toString();

        if (!validate()) {
            onSignupFailed();
            return;
        }

        signupButton.setEnabled(false);

        String[] cliente = new String[]{
                "1", "0", nome, cognome, ragioneSociale, telefono, via, civico, citta, cap, codiceFiscale, piva, "1", email, password
        };

        SignupController signupController = new SignupController(cliente);
        signupController.attachActivityObserver(this);

        try {
            signupController.start("");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        //Questo ordine va rispettato per il corretto funzionamento della Insert (Vedi SqlServerSignup)

    }

    @Override
    public void notifyView(Boolean result) {
        if (result == true) {
            onSignupSuccess();
        } else {
            onSignupFailed();
        }
    }

    public void onSignupSuccess() {
        signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        if (nome.isEmpty() || nome.length() < 3 || nome.length() > 30) {
            editText.setError("Between 3 characters and 30 characters");
            valid = false;
        } else {
            editText.setError(null);
        }

        if (cognome.isEmpty() || cognome.length() < 3 || cognome.length() > 30) {
            cognomeText.setError("Between 3 characters and 30 characters");
            valid = false;
        } else {
            cognomeText.setError(null);
        }

        if (citta.isEmpty() || citta.length() < 3 || citta.length() > 50) {
            cittaText.setError("Between 3 characters and 50 characters");
            valid = false;
        } else {
            cittaText.setError(null);
        }

        if (civico.isEmpty() || civico.length() > 5) {
            civicoText.setError("Between 1 characters and 5 characters");
            valid = false;
        } else {
            civicoText.setError(null);
        }

        if (cap.isEmpty() || cap.length() > 5) {
            capText.setError("Between 1 characters and 5 characters");
            valid = false;
        } else {
            capText.setError(null);
        }

        if (telefono.isEmpty() || telefono.length() < 5 || telefono.length() > 13) {
            telefonoText.setError("Between 5 characters and 13 characters");
            valid = false;
        } else {
            telefonoText.setError(null);
        }

        if (ragioneSociale.isEmpty() || telefono.length() < 5 || telefono.length() > 80) {
            ragioneSocialeText.setError("Between 5 characters and 80 characters");
            valid = false;
        } else {
            ragioneSocialeText.setError(null);
        }

        if (via.isEmpty() || via.length() < 5 || via.length() > 20) {
            viaText.setError("Between 5 characters and 20 characters");
            valid = false;
        } else {
            viaText.setError(null);
        }

        if (codiceFiscale.isEmpty() || codiceFiscale.length() < 5 || codiceFiscale.length() > 16) {
            codiceFiscaleText.setError("Between 5 characters and 16 characters");
            valid = false;
        } else {
            codiceFiscaleText.setError(null);
        }

        if (piva.isEmpty() || piva.length() < 5 || piva.length() > 11) {
            pivaText.setError("Between 5 characters and 11 characters");
            valid = false;
        } else {
            pivaText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("enter a valid email address");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 50) {
            passwordText.setError("between 6 and 50 alphanumeric characters");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    @Override
    public void notifyView(ArrayList value) {

    }

    @Override
    public void connectionFailed() {

    }
}
