package giovanni_esempi.topseafoodmobileapplication.Activities;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.SearchView;

import giovanni_esempi.topseafoodmobileapplication.R;

public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    protected final int PRODUCTSLIST = R.id.index;
    protected final int SHOPPINGCART = R.id.shopping_cart;
    protected final int ORDERS = R.id.credit_cart;
    protected final int MYPROFILE = R.id.user_account;
    protected SearchView searchView;
    protected SearchManager searchManager;
    private NavigationView navigationView;
    private DrawerLayout fullLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    public void setContentView(int layoutResID) {
        fullLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base, null);

        FrameLayout activityContainer = (FrameLayout) fullLayout.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullLayout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        drawerToggle = new ActionBarDrawerToggle(this, fullLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        fullLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Get the SearchView and set the searchable configuration
        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        //searchView.setOnCloseListener(setOnCloseListener());

        return true;
    }


    protected void onCloseSearchWidget() {
        //Remove the keyboard input
        if (searchView != null) {
            searchView.clearFocus();
            searchView.onActionViewCollapsed();
        }

    }

    protected abstract SearchView.OnCloseListener setOnCloseListener();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        android.support.v4.app.Fragment fragment = null;

        Class fragmentClass = null;

        switch (item.getItemId()) {
            case (PRODUCTSLIST): {
                if (!(this.getApplicationContext() instanceof ProductActivity)) {
                    Intent switchActivity = new Intent(getApplicationContext(), ProductActivity.class);
                    startActivity(switchActivity);

                    return true;
                }

            }
            case (SHOPPINGCART): {
                fragmentClass = ShoppingCartFragment.class;
                break;

            }
            case (ORDERS): {
                fragmentClass = OrdersFragment.class;
                break;
            }
             /*
            case (MYPROFILE): {
                if (!(this.getApplicationContext() instanceof ClienteActivity)) {
                    Intent switchActivity = new Intent(getApplicationContext(), ClienteActivity.class);
                    startActivity(switchActivity);
                    break;
                }*/
        }

        try {
            fragment = (android.support.v4.app.Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.inseriscimi, fragment).commit();
        return true;

    }

}

