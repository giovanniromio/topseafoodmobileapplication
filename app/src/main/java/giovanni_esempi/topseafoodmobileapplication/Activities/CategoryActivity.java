package giovanni_esempi.topseafoodmobileapplication.Activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;

import java.sql.SQLException;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.Controller.CategoryController;
import giovanni_esempi.topseafoodmobileapplication.Controller.Controller;
import giovanni_esempi.topseafoodmobileapplication.DataAdapter.CategoryAdapter;
import giovanni_esempi.topseafoodmobileapplication.Models.Category;
import giovanni_esempi.topseafoodmobileapplication.R;

public class CategoryActivity extends BaseActivity implements Controller.ActivityObserver<Category> {

    private CategoryAdapter adapter;

    private GridView gridView;

    private ProgressDialog progressDialog;

    private CategoryController categoryController;

    private String queryAllCategories = "SELECT * FROM mycompany.dbo.CATEGORIA";

    private String querySearchCategoriesRadix = "SELECT * FROM dbo.CATEGORIA WHERE Descrizione LIKE '%";

    private String querySearchCategoriesEnding = "%'";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        gridView = (GridView) findViewById(R.id.gridView);

        categoryController = new CategoryController();
        categoryController.attachActivityObserver(this);
        //Listener for intenet on the UI
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (query != null) {
                doMySearch(query);
                onCloseSearchWidget();
            }
        } else {
            try {
                categoryController.start(queryAllCategories);
                loadCategories();
            } catch (SQLException e) {
                //TODO ADD TOAST MAKE TEST
                e.printStackTrace();
            }
        }
    }

    private void doMySearch(String query) {
        try {
            categoryController.start(querySearchCategoriesRadix + query + querySearchCategoriesEnding);
            loadCategories();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadCategories() {
        progressDialog = new ProgressDialog(CategoryActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Caricamento categorie...");
        progressDialog.show();
    }

    private void populateGridView(ArrayList<Category> values) {
        if (adapter == null) {
            adapter = new CategoryAdapter(this, values);
            gridView.setAdapter(adapter);
        } else {
            adapter.clear();
            adapter.addAll(values);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void notifyView(ArrayList<Category> values) {
        populateGridView(values);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category) gridView.getItemAtPosition(position);
                Intent switchToProducts = new Intent(CategoryActivity.this, ProductActivity.class);
                switchToProducts.putExtra("IDCATEGORY", selectedCategory.getID());
                startActivity(switchToProducts);
            }
        });

        progressDialog.dismiss();

    }

    @Override
    protected SearchView.OnCloseListener setOnCloseListener() {
        return new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                try {
                    //Display all the categories
                    categoryController.start(queryAllCategories);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                onCloseSearchWidget();
                return true;
            }
        };
    }

    @Override
    public void notifyView(Boolean result) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void connectionFailed() {
    }

}
