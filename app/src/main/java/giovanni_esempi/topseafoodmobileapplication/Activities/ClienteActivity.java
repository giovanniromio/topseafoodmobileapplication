package giovanni_esempi.topseafoodmobileapplication.Activities;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.SearchView;

import butterknife.Bind;
import butterknife.ButterKnife;
import giovanni_esempi.topseafoodmobileapplication.DataAdapter.DataManager;
import giovanni_esempi.topseafoodmobileapplication.Models.ClienteFornitore;
import giovanni_esempi.topseafoodmobileapplication.R;

public class ClienteActivity extends BaseActivity {

    //Aggiungere tutti i campi

    @Bind(R.id.nomeText)
    EditText nomeText;
    @Bind(R.id.cognomeText)
    EditText cognomeText;
    @Bind(R.id.capText)
    EditText capText;
    @Bind(R.id.cfText)
    EditText cfText;
    @Bind(R.id.emailText)
    EditText emailText;
    @Bind(R.id.passwordText)
    EditText passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cliente);
        ButterKnife.bind(this);

        populateFields();
    }

    protected void populateFields() {
        ClienteFornitore cliente = DataManager.getInstanceOf().getCliente();
        nomeText.setText(cliente.getNome_referente());
        cognomeText.setText(cliente.getCognome_referente());
        emailText.setText(cliente.getUsername());
        passwordText.setText(cliente.getPassword());
        capText.setText(cliente.getCAP());
        cfText.setText(cliente.getCodice_fiscale());
    }

    @Override
    protected SearchView.OnCloseListener setOnCloseListener() {
        return null;
    }
}
