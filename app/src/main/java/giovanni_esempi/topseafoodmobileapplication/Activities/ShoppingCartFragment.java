package giovanni_esempi.topseafoodmobileapplication.Activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;

import giovanni_esempi.topseafoodmobileapplication.Controller.Controller;
import giovanni_esempi.topseafoodmobileapplication.Controller.ShoppingCartController;
import giovanni_esempi.topseafoodmobileapplication.DataAdapter.ConfectionAdapter;
import giovanni_esempi.topseafoodmobileapplication.DataAdapter.DataManager;
import giovanni_esempi.topseafoodmobileapplication.R;

public class ShoppingCartFragment extends android.support.v4.app.Fragment implements Controller.ActivityObserver {

    private ListView listView;

    private TextView txtViewAmount;

    private Button sendOrder;

    private ConfectionAdapter confectionAdapter;

    private ShoppingCartController shoppingCartController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        FragmentActivity faActivity = (FragmentActivity) super.getActivity();
        LinearLayout llLayout = (LinearLayout) inflater.inflate(R.layout.activity_shopping_cart, container, false);

        listView = (ListView) llLayout.findViewById(R.id.listView2);
        txtViewAmount = (TextView) llLayout.findViewById(R.id.textView_ammontare);
        sendOrder = (Button) llLayout.findViewById(R.id.button2);

        sendOrder.setOnClickListener(sendOrder());

        shoppingCartController = new ShoppingCartController();
        shoppingCartController.attachActivityObserver(this);

        populateListView();

        return llLayout;

    }


    private View.OnClickListener sendOrder() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //The controller build the query by himself
                    shoppingCartController.start("");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void populateListView() {
        if (confectionAdapter == null) {
            confectionAdapter = new ConfectionAdapter(super.getActivity().getApplicationContext(), DataManager.getInstanceOf().getConfezioniInCarrello());
            listView.setAdapter(confectionAdapter);

            int cont = 0;
            for (int i = 0; i < confectionAdapter.getCount(); i++) {
                cont += confectionAdapter.getItem(i).getTotale();
            }

            txtViewAmount.setText("" + cont + " €");

        }
    }

    @Override
    public void notifyView(ArrayList value) {

    }

    @Override
    public void notifyView(Boolean result) {
        Toast.makeText(super.getActivity().getApplicationContext(), "Ordine spedito", Toast.LENGTH_LONG).show();
    }

    @Override
    public void connectionFailed() {

    }

}
